package org.davinci.aspectodos;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Numeros {
    private static int numero;
    private static ArrayList<String> word = new ArrayList<String>();
    private static ArrayList<String> wordTemp = new ArrayList<String>();
    private static ArrayList<String> letterTemp = new ArrayList<String>();
    private static FileWriter file;

    public static String letters[][] = {
            {"2", "A", "B", "C"},
            {"3", "D", "E", "F"},
            {"4", "G", "H", "I"},
            {"5", "J", "K", "L"},
            {"6", "M", "N", "O"},
            {"7", "P", "R", "S"},
            {"8", "T", "U", "V"},
            {"9", "W", "X", "Y"}
    };

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Ingresa el número de 7 dígitos");
        String telefono = teclado.next();
        try {
            file = new FileWriter(System.getProperty("user.dir") + "/src/main/resources/test.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        char tel[];
        tel = telefono.toCharArray();

        try {
            if (tel.length != 7)
                throw new RuntimeException("Longitud no válida");
            for (int i = 0; i <= tel.length - 1; i++) {
                numero = Integer.parseInt(String.valueOf(tel[i]));
                if (numero == 0 || numero == 1)
                    throw new RuntimeException("numeros entre 2 y 9");
            }

            for (int i = 0; i <= tel.length - 1; i++) {
                Buscaword(Integer.parseInt(String.valueOf(tel[i])));

                if (word.isEmpty()) {
                    word = (ArrayList) letterTemp.clone();
                } else {
                    String a;
                    wordTemp = (ArrayList) word.clone();
                    for (int k = 0; k < word.size(); k++) {
                        a = word.get(k);
                        wordTemp.remove(a);
                        for (int l = 0; l < letterTemp.size(); l++) {
                            wordTemp.add(a + letterTemp.get(l));
                        }
                    }
                    word = (ArrayList) wordTemp.clone();
                }
            }

            for (int o = 0; o < word.size(); o++)
                writeFile(wordTemp.get(o));

            file.close();

        } catch (Exception e) {

            System.out.println(e.getMessage());
        }
    }

    private static void writeFile(String word) {
        try {
            file.write(word);
            file.write(System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void Buscaword(int num) {
        int val;
        String letrastmp[];
        letterTemp.clear();

        for (int i = 0; i <= letters.length - 1; i++) {
            val = Integer.parseInt(String.valueOf(letters[i][0]));
            if (num == val)
                for (int j = 0; j <= letters[i].length - 1; j++) {

                    if (j > 0) {
                        letterTemp.add(letters[i][j]);
                    }
                }
        }
    }
}
