package org.davinci.aspectouno;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class InventarioFarmacia {

    public static void main(String args[])throws IOException{
        InventarioFarmacia prueba = new InventarioFarmacia();

        File archivo = prueba.createFile("Test");
        prueba.fileWrite(archivo,prueba.read());
    }

    public void fileWrite(File file, int productos) throws IOException{
        FileWriter writer = new FileWriter(file);
        for(int i=0;i<productos;i++)
        {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Código del producto:");
            writer.write(scanner.nextLine());
            writer.write(System.lineSeparator());
            System.out.println("Precio del producto:");
            writer.write(scanner.nextLine());
            writer.write(System.lineSeparator());
            System.out.println("Descripción:");
            writer.write(scanner.nextLine());
            writer.write(System.lineSeparator());
            System.out.println("------------------------------------------");
            writer.write("------------------------------------------");
        }

        writer.flush();
        writer.close();

    }

    public int read(){
        System.out.println("***--------------------- Sistema de Inventario ---------------------***");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Indica el número de productos que deseas agregar: ");
        int entrada = 1;
        boolean condicion = false;

        while(condicion == false) {
            try{
                String texto = scanner.next();
                entrada = Integer.parseInt(texto);
                condicion = true;
            }
            catch(Exception e){
                System.out.println("Entrada Incorrecta " + e.getMessage());
            }
        }
        return entrada;
    }

    public File createFile(String nombre){
        File file = new File(nombre+".txt");
        try{
            file.createNewFile();
        }
        catch(Exception ex){
            System.out.println("Error al crear archivo " + ex.getMessage());
        }

        return file;
    }
}
