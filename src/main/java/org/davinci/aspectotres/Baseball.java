package org.davinci.aspectotres;

public class Baseball extends Sport {
    Baseball play() {
        System.out.print("baseball ");
        return new Baseball();
    }

    Sport play(int x) {
        System.out.print("sport ");
        return new Sport();
    }

    public static void main(String[] args) {
        new Baseball().play();
        new Baseball().play(7);
        //super.play(7);
        new Sport().play();
        Sport s = new Baseball();
        s.play();

    }
}
