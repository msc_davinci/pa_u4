package org.davinci.aspectotres;

public class Sport {
    Sport play() {
        System.out.print("play ");
        return new Sport();
    }

    Sport play(int x) {
        System.out.print("play x ");
        return new Sport();
    }
}
